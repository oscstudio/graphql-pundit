# frozen_string_literal: true

module GraphQL
  module Pundit
    VERSION = '0.8'
  end
end
